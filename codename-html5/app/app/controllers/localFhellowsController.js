(function () {
    var localFhellowsController = function ($scope, $rootScope, $queries, $interests, $chat, location, appConstants) {
        $scope.imagePath = "static/img/public-images/";
        $scope.filters = {location: '', proximity: 200, type: "", search: ""};
        $scope.filtersType = [];
        $scope.filtersLookingTo = [];
        $scope.tags = [];
        $scope.tagsText = [];
        $scope.serverUrlFull = appConstants.server + appConstants.context;

        location.get(angular.noop, angular.noop);


        $scope.selectAddress = function () {
            console.log($scope.lookedUpLocation);
            if ($scope.lookedUpLocation) {
                $scope.selectedLocation = $scope.lookedUpLocation;
                $scope.loadFhellowsByLocation($scope.selectedLocation.longitude, $scope.selectedLocation.latitude,
                        $scope.tagsText, $scope.filtersLookingTo, $scope.filtersType);
            }
        };

        $scope.interestsTagAdded = function ($tag) {

            $scope.tagsText.push($tag.text);


            if ($scope.lookedUpLocation) {
                $scope.loadFhellowsByLocation( $scope.selectedLocation.longitude, $scope.selectedLocation.latitude, $scope.tagsText, $scope.filtersLookingTo, $scope.filtersType);
            } else {
                $scope.loadFhellows($scope.tagsText, $scope.filtersLookingTo, $scope.filtersType);

            }
        };
        $scope.interestsTagRemoved = function ($tag) {

            $scope.tagsText.splice($scope.tagsText.indexOf($tag.text), 1);

            if ($scope.lookedUpLocation) {
                $scope.loadFhellowsByLocation( $scope.selectedLocation.longitude, $scope.selectedLocation.latitude, $scope.tagsText, $scope.filtersLookingTo, $scope.filtersType);
            } else {
                $scope.loadFhellows($scope.tagsText, $scope.filtersLookingTo, $scope.filtersType);

            }
        };

        $scope.$watch('lookedUpLocation', $scope.selectAddress);

        $scope.newConversation = function (selectedUser) {
            console.log("About to create New conversation for "+selectedUser);
            $chat.newConversation(selectedUser).success(function (data) {
                
                console.log("New conversation created ");
                console.log(data);
                $rootScope.$broadcast('goTo', "/messages/"+data);
                
                
            }).error(function (data) {
                console.log("Error: ");
                console.log(data);
                $rootScope.$broadcast("quickNotification", "Something went wrong creating a new conversations!" + data);
            });

        }



        $scope.loadInterests = function ($query) {
            return $scope.interests.filter(function (interest) {
                return interest.text.toLowerCase().indexOf($query.toLowerCase()) != -1;
            });

        }

        $scope.loadInterestOnInit = function () {
            $interests.getAll().success(function (data) {
                //$rootScope.$broadcast("quickNotification", "Clubs loaded!");
                console.log("Interests loaded: ");

                $scope.interests = data;
                console.log($scope.interests);
            }).error(function (data) {
                console.log("Error: ");
                console.log(data);
                $rootScope.$broadcast("quickNotification", "Something went wrong loading the interests!" + data);
            });

        }
        $scope.loadInterestOnInit();
        $scope.typeButtonPressed = function (buttonName) {

            if ($scope.filtersType.indexOf(buttonName) == -1) {
                $scope.filtersType.push(buttonName);
            } else {
                $scope.filtersType.splice($scope.filtersType.indexOf(buttonName), 1);
            }
            console.log($scope.filtersType);
            if ($scope.lookedUpLocation) {

                $scope.loadFhellowsByLocation($scope.selectedLocation.longitude, $scope.selectedLocation.latitude, $scope.tagsText, $scope.filtersLookingTo, $scope.filtersType);
            } else {
                $scope.loadFhellows($scope.tagsText, $scope.filtersLookingTo, $scope.filtersType);

            }
        }
        $scope.lookingToButtonPressed = function (buttonName) {

            if ($scope.filtersLookingTo.indexOf(buttonName) == -1) {
                $scope.filtersLookingTo.push(buttonName);
            } else {
                $scope.filtersLookingTo.splice($scope.filtersLookingTo.indexOf(buttonName), 1);
            }
            console.log($scope.filtersLookingTo);
            if ($scope.lookedUpLocation) {

                $scope.loadFhellowsByLocation($scope.selectedLocation.longitude, $scope.selectedLocation.latitude,
                        $scope.tagsText, $scope.filtersLookingTo, $scope.filtersType);
            } else {
                $scope.loadFhellows($scope.tagsText, $scope.filtersLookingTo, $scope.filtersType);
            }
        }

        $scope.loadFhellowsByLocation = function (lon, lat, tags, lookingFors, categories) {
            console.log("lon = " + lon);
            console.log("lat = " + lat);
            console.log("lookingFors = ");
            console.log(lookingFors);
            console.log("categories = ");
            console.log(categories);
            $queries.getByLocation(lon, lat, tags, lookingFors, categories).success(function (data) {
                //$rootScope.$broadcast("quickNotification", "Clubs loaded!");
                console.log("Fhellows: ");
                console.log(data);
                $scope.fhellowsList = data;
                console.log($scope.fhellowsList);
            }).error(function (data) {
                console.log("Error: ");
                console.log(data);
                $rootScope.$broadcast("quickNotification", "Something went wrong!" + data);
            });

        };


        $scope.loadFhellows = function (tags, lookingFors, categories) {


            $queries.getAll(tags, lookingFors, categories).success(function (data) {
                //$rootScope.$broadcast("quickNotification", "Clubs loaded!");
                console.log("Fhellows: ");
                console.log(data);
                $scope.fhellowsList = data;
                console.log($scope.fhellowsList);
            }).error(function (data) {
                console.log("Error: ");
                console.log(data);
                $rootScope.$broadcast("quickNotification", "Something went wrong!" + data);
            });

        };

        $scope.loadFhellows($scope.tagsText, $scope.filtersLookingTo, $scope.filtersType);







        $(window).scroll(function () {

            if ($(window).scrollTop() > 235) {
                $(".sticky").addClass("isSticky");
            } else {
                $(".sticky").removeClass("isSticky");
            }
        });

    };

    localFhellowsController.$inject = ['$scope', '$rootScope', '$queries', '$interests', '$chat','location', 'appConstants'];
    angular.module("codename").controller("localFhellowsController", localFhellowsController);

}());


